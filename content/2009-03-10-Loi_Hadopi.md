Title: "Loi Hadopi"
modifier: "Daniel"
tags: net
Status: draft

Message-ID: <49B57A27.6040907@xxx>
Date: Mon, 09 Mar 2009 21:20:55 +0100
From: myself <xxx@xxx>
User-Agent: Thunderbird 2.0.0.19 (X11/20090107)
MIME-Version: 1.0
To: xxx@assemblee-nationale.fr
CC: xx@laquadrature.net
Subject: Loi Hadopi
X-Enigmail-Version: 0.95.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit

Monsieur le Député,
Je me doute que vous avez de nombreux sujets à traiter actuellement,
mais l'actualité nous ramène à la loi Hadopi qui devrait être traitée
demain à l'assemblée.

Rapidement, je ne suis pas téléchargeur, mais je me souviens avoir été,
adolescent, découvreur compulsif d'oeuvres musicales, d'avoir écumé
bibliothèques et médiathèques, et d'avoir recopié bon nombre d'oeuvres
afin de les réécouter à l'envi, faire découvrir mes passions à mes amis,
 appris à retranscrire les morceaux écoutés, etc...
Les retombées économiques, outre l'achat de cassettes vierges,
lourdement taxées en France, pour le bénéfice de grandes vedettes du
misic hall que je n'écoutais pas, a donné lieu par ailleurs à l'achat de
divers instruments de musique, neufs et d'occasion, de méthodes de
musique, l'achat de billets de concerts, de disques, pour les artistes
qui me plaisaient le plus, de participation à divers festivals dans
notre belle région bretonne, etc ...
Bref, j'ai simplement modestement développé ce que l'on appelle une
forme de culture.

Par ailleurs, les mesures d'application de ladite loi semblent porter
gravement atteinte au formidable espace de liberté qu'est encore
internet, pour prendre la voie de pays comme la chine ou l'iran. Alors
qu'il y a déjà fort à faire pour lutter contre les influences de grands
groupes multinationaux allant contre la neutralité du net, voilà que nos
députés laisseraient passer ce genre de loi arriérée et liberticide,
tout juste bonne à prolonger un peu plus le plan marketing dépassé des
industries du divertissement (pour quelque temps encore).

S'il vous plait, réagissez !
Je sais que vous avez  de nombreux autres sujets similaires à traiter
depuis quelques mois, mais je vous assure de mon soutien entier contre
ce type de lois inutiles, couteuses, et masquant le principal  :
l'intérêt du bien public.

Veuillez agréer, Monsieur le Député, l'assurance de mes salutations
distinguées.
