Title: Compilation des liens de la semaine 2018-05-13
Date:  2018-05-13
Tags:  veille, kubernetes, druid, kafka, machinelearning, docker, scala, spark, angular, container
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine



# [Spark](http://stream.logpickr.com/profile/daniel?_t=rss/tag/spark)
Self-Paced Apache Spark Training from Databricks - Databricks
[https://databricks.com/training-overview/training-self-paced](https://databricks.com/training-overview/training-self-paced)



# [Machine Learning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/machinelearning)
Build and Deploy Scalable Machine Learning in Production with Kafka
[https://www.confluent.io/blog/build-deploy-scalable-machine-learning-production-apache-kafka](https://www.confluent.io/blog/build-deploy-scalable-machine-learning-production-apache-kafka)  _[#kafka](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kafka)_  





# [Docker](http://stream.logpickr.com/profile/daniel?_t=rss/tag/docker)
.docker
[https://bersace.cae.li/dnsdock.html](https://bersace.cae.li/dnsdock.html)



# Autres
Introducing the Operator Framework: Building Apps on Kubernetes
[https://coreos.com/blog/introducing-operator-framework](https://coreos.com/blog/introducing-operator-framework)  _[#kubernetes](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kubernetes)_  

Druid
[https://www.slideshare.net/doriwaldman/druid-88876307?trk=v-feed](https://www.slideshare.net/doriwaldman/druid-88876307?trk=v-feed)  _[#druid](http://stream.logpickr.com/profile/daniel?_t=rss/tag/druid)_  

Our journey with druid - from initial research to full production sca…
[https://www.slideshare.net/ItaiYaffe/our-journey-with-druid-from-initial-research-to-full-production-scale](https://www.slideshare.net/ItaiYaffe/our-journey-with-druid-from-initial-research-to-full-production-scale)  _[#druid](http://stream.logpickr.com/profile/daniel?_t=rss/tag/druid)_  

Feuille de route de Scala 3.0 : il utilisera le compilateur Dotty afin de simplifier la structure du langage, Scala 3.0 est attendu en 2020
[]()  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  

Building maintainable Angular applications – Versett – Medium
[https://medium.com/curated-by-versett/building-maintainable-angular-2-applications-5b9ec4b463a1](https://medium.com/curated-by-versett/building-maintainable-angular-2-applications-5b9ec4b463a1)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  

Titus, the Netflix container management platform, is now open source
[https://medium.com/netflix-techblog/titus-the-netflix-container-management-platform-is-now-open-source-f868c9fb5436](https://medium.com/netflix-techblog/titus-the-netflix-container-management-platform-is-now-open-source-f868c9fb5436)  _[#container](http://stream.logpickr.com/profile/daniel?_t=rss/tag/container)_  

Angular Push Notifications: a Complete Step-by-Step Guide
[https://blog.angular-university.io/angular-push-notifications](https://blog.angular-university.io/angular-push-notifications)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_