Title: Compilation des liens de la semaine 2018-11-25
Date:  2018-11-25
Tags:  veille, docker, performance, web, front, spark, machinelearning
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine



# [Spark](http://stream.logpickr.com/profile/daniel?_t=rss/tag/spark)

[]()  _[#machinelearning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/machinelearning)_  







# [Docker](http://stream.logpickr.com/profile/daniel?_t=rss/tag/docker)
Traefik et Docker
[https://blog.wecoprod.com/traefik-docker](https://blog.wecoprod.com/traefik-docker)



# Autres

[https://blog.octo.com/introduire-une-culture-de-la-performance-web-sur-son-projet/](https://blog.octo.com/introduire-une-culture-de-la-performance-web-sur-son-projet/)  _[#performance](http://stream.logpickr.com/profile/daniel?_t=rss/tag/performance)_    _[#web](http://stream.logpickr.com/profile/daniel?_t=rss/tag/web)_    _[#front](http://stream.logpickr.com/profile/daniel?_t=rss/tag/front)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_