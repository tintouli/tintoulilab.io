Title: Compilation des liens de la semaine 2019-09-08
Date:  2019-09-08
Tags:  veille, graph, algorithm, scala, keycloak, kafka, mysql, cdc
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine











# Autres
Comment élaborer un planning ? Avec des crayons de couleur, de la patience et… des mathématiques
[https://theconversation.com/comment-elaborer-un-planning-avec-des-crayons-de-couleur-de-la-patience-et-des-mathematiques-122373](https://theconversation.com/comment-elaborer-un-planning-avec-des-crayons-de-couleur-de-la-patience-et-des-mathematiques-122373)  _[#graph](http://stream.logpickr.com/profile/daniel?_t=rss/tag/graph)_    _[#algorithm](http://stream.logpickr.com/profile/daniel?_t=rss/tag/algorithm)_  

keycloak4s
[https://index.scala-lang.org/fullfacing/keycloak4s/keycloak4s-admin/1.1.0?target=_2.12](https://index.scala-lang.org/fullfacing/keycloak4s/keycloak4s-admin/1.1.0?target=_2.12)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_    _[#keycloak](http://stream.logpickr.com/profile/daniel?_t=rss/tag/keycloak)_  

MySQL CDC with Apache Kafka and Debezium. - Clairvoyant Blog
[https://blog.clairvoyantsoft.com/mysql-cdc-with-apache-kafka-and-debezium-3d45c00762e4](https://blog.clairvoyantsoft.com/mysql-cdc-with-apache-kafka-and-debezium-3d45c00762e4)  _[#kafka](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kafka)_    _[#mysql](http://stream.logpickr.com/profile/daniel?_t=rss/tag/mysql)_    _[#cdc](http://stream.logpickr.com/profile/daniel?_t=rss/tag/cdc)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_