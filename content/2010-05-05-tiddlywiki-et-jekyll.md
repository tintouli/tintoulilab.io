Title: Tiddlywiki et Jekyll
tags: jekyll, site, ruby, tiddlywiki, todo
Status: draft

Je suis un fan de longue date de "TiddlyWiki":http://www.tiddlywiki.com : un wiki en javascript dans un seul fichier HTML !
Idéal pour faire un bloc note, toujours disponible sur une clé USB ou en ligne.
Et même si je publiais des choses dans un CMS (genre dotclear, spip, ...) je gardais toujours une copie originale dans un de mes nombreux tidllywiki sur clé USB.
Il exite même un "plugin":http://boycook.wordpress.com/2007/10/11/tiddlyblogger-is-here/ pour publier à partir de TiddlyWiki vers un CMS genre dotclear ou wordpress qui supporte le protocole XML/RPC.
Maintenant que j'ai découvert "jekyll":/tag/jekyll, je peux exporter facilement tout ou partie d'un TiddlyWiki vers des fichiers au format attendu par jekyll (textile par exemple).
je me suis inspiré pour ça de l'outil de gestion de TiddlyWiki qui permet d'exporter chaque tiddler (un unité de texte sous TiddlyWiki) en un fichier. L'outil s'appelle "ginsu":http://trac.tiddlywiki.org/wiki/Ginsu et est codé en ... "_Ruby_":http://trac.tiddlywiki.org/browser/Trunk/tools/cooker ! Cela a donc été le facteur déclencheur pour apprendre ce langage que je lorgnais depuis longtemps. 
Pour un ancien smalltalker, cela n'a pas été trop difficile, tellement Ruby est inspiré (au grand sens du terme) par _Smalltalk_.
* "Le code se trouve là":http://github.com/danc/tiddly2jekyll
Reste un inconvénient, les conventions de marquage des wikis (et celle utilisée par tiddlywiki) n'est pas complètement en phase avec celle utilisée de base par jekyll (textile ou markdown). 
Qu'à cela ne tienne, il semble possible de spécifier et d'utiliser d'autres modèles de marquages, fichiers par fichiers, et de rendre correctement sous jekyll une page issue directement de TiddlyWiki ! (à faire ...)
