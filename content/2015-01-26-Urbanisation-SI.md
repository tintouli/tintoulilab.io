Title: Urbanisation du SI
Status: draft

# Urbanisation SI

* Enjeux 
* Critères de réussite
* Définition du périmètre

## Etat des lieux

* Nombre d'applications
* Interlocuteurs sur chacune des applications
    * Nature (MOA, MOE, expert, DBA, ...)
* Existence de compétences transverses
* Types et nombres d'utilisateurs pour chacune des applications

### Architecture
* Existence d'un bus de données SOA
    * EAI
    * ESB
* Silos applicatifs
* Référentiel de données MDM

### Documentations

* Documentations internes (MCD/MPD des bases de données)
* Documentations externes (interfaces, guide d'utilisation)
* Types d'interfaces
    * Fichier plat
    * Web service
    * format interne
    * ...
* Informations techniques sur les différentes applications
* Cartographie des applications et de leurs interactions
* Description des règles de gestion
    * Règles métier
    * Règles de validation
    * Nomenclatures existantes applicables aux données (dictionnaires de valeurs, ...)
* Flux entrants / sortants
    * Matrices de flux
* Criticité et fréquence de mise à jour des données
    * Aspects temps réel / batch / ...

  
### Aspects contractuels
* Nature de l'hébergement et de l'expertise
    * Interne
    * Externalisé
    * Cloud
* SLA des différentes applications
* Cartographie des plateformes existantes
    * QA, UAT, PREPROD, PROD
* Politique de sauvegarde / restauration des données
