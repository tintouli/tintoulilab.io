Title: Spark, un framework polyvalent
tags:  spark, hadoop, flink
Category: bigdata

Au début de l'ère du Big Data, vous disposiez de différentes bibliothèques pour réaliser diverses fonctions :

* chargement des données (Sqoop)
* Batches Map Reduce (via Pig)
* Requêtage de type SQL (Hive)
* Stream Processing
* Machine Learning (Mahout)
* ...

Différents langages et outils étaient nécessaires pour installer et mettre en oeuvre ces fonctionnalités.

De plus, le stockage de l'information était principalement envisagé sous HDFS, et la gestion de la répartition et l'ordonnacement des calculs par les frameworks Yarn et Zookeeper.

Spark vient bousculer cette immense ménagerie pleine d'animaux étranges, et propose d'autres options de gestion, de stockage, tout en assurant 

* une possible continuité avec l'écosystème Hadoop (votre investissement n'est pas perdu), 
* mais en permettant désormais de s'en affranchir (on peut faire du Big Data sans Hadoop, contrairement à une idée communément répandue)

Le ticket d'entrée n'est pas très cher, et se rentabilise assez vite :

* Accessible via des APIs permettant la programmation en Scala, Java, Python
* il donne accès à différentes librairies de traitement, 
* permet de faire du Map Reduce et plus, 
* du machine learning (tout Mahout bascule progressivement en Spark)
* du traitement en streaming
* SparkSQL remplace avantageusement Hive
* SparkR (depuis la v1.4) se rapproche du monde des data analystes familiers du langage R
* peut tourner en standalone (mise au point aisée), sur Yarn, Mesos, amazon EC2

L'implication récente d'IBM dans l'écosystème Spark donne davantage de gages à cette technologie, flexible, homogène, polyvalente.

Flink est une technologie très similaire en plein développement, (issu de recherche d'équipes berlinoises), qui semble offrir quelques avantages par rapport à Spark : à suivre, le passage de Spark à Flink ne devrait pas poser de gros problèmes (mêmes langages, principes similaires de traitement en RAM, avec un axe plus natif sur le streaming pour Flink).
