Title:  Un bookmarklet pour le Journal du hacker 
tags: journalduhacker, astuce


Vous voulez devenir un contributeur important du Jdh ? Vous avez plein de liens à partager ? Vous trouvez que la publication d'un article demande trop de clics  ?

C'est vrai que sans outillage c'est un peu long : 

- aller sur l'article, 
- copier l'url, 
- aller sur la page de publication du Jdh, 
- coller l'url, 
- prendre le titre, 
- définir les marques associées, 
- poster .... 
- et puis on recommence

Le **[bookmarklet](https://fr.wikipedia.org/wiki/Bookmarklet)** du Jdh est fait pour vous !

Car il existe bien un bookmarklet officiel, mais il est tellement bien caché que l'ami Cascador (grand contributeur du Jdh s'il en est) était passé à côté : 
Quand je lui ai montré, il a failli en tomber de sa chaise (enfin je pense).

Alors je ne vous fais pas languir plus longtemps : vous aussi publiez à la vitesse de l'éclair avec le bookmarklet du Jdh, qui se trouve caché dans la page [soumettre une info](https://www.journalduhacker.net/stories/new), mais nécessite, pour apparaître que vous ayez cliqué sur le lien interne "Lignes directrices de la proposition d'une nouvelle"

C'est le gros bouton **'Soumettre à Journal du Hacker'** comme sur la copie d'écran :  ![bookmarklet](https://blog.journalduhacker.net/data/medias/bookmarklet.png)

Drag and drop de ce bouton dans votre barre de bookmarks, ou clic droit/ Marque page sur ce lien : vous êtes prêts ! 

Maintenant, lorsque vous êtes sur une page intéressante à partager il vous suffit de cliquer sur ce bookmark pour arriver sur la page de partage du Jdh avec toutes les infos pré-saisies : il ne vous reste qu'à renseigner les marques.

**Ne rêvez pas** : vous ne rattrapperez pas Cascador en terme de contribution, mais vous publierez plus facilement et agréablement.

Ne vous laissez pas emporter : on préfèrera toujours la **qualité** à la *quantité*.

Bonnes contributions !

PS : le code du bookmarklet, si vous souhaitez créer votre bookmark à la main :

```javascript
javascript:window.location=%22http://journalduhacker.net/stories/new?url=%22+encodeURIComponent(document.location)+%22&title=%22+encodeURIComponent(document.title)
```
