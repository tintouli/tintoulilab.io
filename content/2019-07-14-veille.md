Title: Compilation des liens de la semaine 2019-07-14
Date:  2019-07-14
Tags:  veille, docker, container, kafka, spark, test, python, postgresql, deeplearning, machinelearning, scala, notebook, datascience
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine



# [Spark](http://stream.logpickr.com/profile/daniel?_t=rss/tag/spark)
Les tests avec Spark : sortir la tête de l'eau · Univalence blog
[https://blog.univalence.io/les-tests-avec-spark-sortir-la-tete-de-leau](https://blog.univalence.io/les-tests-avec-spark-sortir-la-tete-de-leau)  _[#test](http://stream.logpickr.com/profile/daniel?_t=rss/tag/test)_  



# [Machine Learning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/machinelearning)
MXNet: A Scalable Deep Learning Framework
[https://mxnet.apache.org](https://mxnet.apache.org)  _[#deeplearning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/deeplearning)_  

Deeplearning4j
[https://deeplearning4j.org](https://deeplearning4j.org)  _[#deeplearning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/deeplearning)_  





# [Docker](http://stream.logpickr.com/profile/daniel?_t=rss/tag/docker)
Pourquoi Cri-O est-il un bon remplaçant de Docker ? Par Akram Blouza - Speaker Deck
[https://speakerdeck.com/wescale/pourquoi-cri-o-est-il-un-bon-remplacant-de-docker-par-akram-blouza](https://speakerdeck.com/wescale/pourquoi-cri-o-est-il-un-bon-remplacant-de-docker-par-akram-blouza)  _[#container](http://stream.logpickr.com/profile/daniel?_t=rss/tag/container)_  


# [Science des données](http://stream.logpickr.com/profile/daniel?_t=rss/tag/datascience)
Jupyter is the new Excel (but not for your boss) – Towards Data Science
[https://towardsdatascience.com/jupyter-is-the-new-excel-but-not-for-your-boss-d24340ebf314](https://towardsdatascience.com/jupyter-is-the-new-excel-but-not-for-your-boss-d24340ebf314)  _[#notebook](http://stream.logpickr.com/profile/daniel?_t=rss/tag/notebook)_  

Retour sur la matinée Modern data-science avec Dataiku et Azure Databricks
[https://medium.com/@paul.peton/retour-sur-la-matin%C3%A9e-modern-data-science-avec-dataiku-et-azure-databricks-1f0b6367161](https://medium.com/@paul.peton/retour-sur-la-matin%C3%A9e-modern-data-science-avec-dataiku-et-azure-databricks-1f0b6367161)  _[#machinelearning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/machinelearning)_  


# Autres
Kafka et les groupes de consommateurs · Univalence blog
[https://blog.univalence.io/kafka-et-les-groupes-de-consommateurs](https://blog.univalence.io/kafka-et-les-groupes-de-consommateurs)  _[#kafka](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kafka)_  

Fastest Way to Load Data Into PostgreSQL Using Python | Haki Benita
[https://hakibenita.com/fast-load-data-python-postgresql](https://hakibenita.com/fast-load-data-python-postgresql)  _[#python](http://stream.logpickr.com/profile/daniel?_t=rss/tag/python)_    _[#postgresql](http://stream.logpickr.com/profile/daniel?_t=rss/tag/postgresql)_  

Keep your projects up-to-date with Scala Steward | The Scala Programming Language
[https://www.scala-lang.org/blog/2019/07/10/announcing-scala-steward.html](https://www.scala-lang.org/blog/2019/07/10/announcing-scala-steward.html)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  

kafka4s
[https://banno.github.io/kafka4s](https://banno.github.io/kafka4s)  _[#kafka](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kafka)_    _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_