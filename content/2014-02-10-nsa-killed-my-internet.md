Title: NSA Killed my internet
tags: internet

... maintenant, il nous faut en reconstruire un ...

![cat-unhappy](/images/gallery/2014/Feb/NSA_killed.jpg)

Par exemple, en vous autohébergeant.

D'accord, c'est pas facile.

Plus de pistes sur le sujet, bientôt.

### Pour commencer :

- [Owncloud](http://owncloud.org/)
- [Cozy cloud](https://www.cozycloud.cc/)
- [Yunohost](http://yunohost.org)

### Matériels :

De faible consommation, à laisser tourner en permanence, à alimenter en solaire :

- [Raspberry PI](http://www.raspberrypi.org/)
- [Cubox](http://solid-run.com/products/cubox)