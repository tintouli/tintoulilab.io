date: 2010-12-07  
tags: flex, maven, sonar , nexus, slide, hudson
title: Slides industrialisation de la production logicielle  

Quelques slides présentant les réflexions en cours sur la production de
logiciel, et des outils pour l’améliorer :

[/slide/maven-nexus-sonar/](/slide/maven-nexus-sonar/)

Au menu :  
 \* hudson  
 \* sonar  
 \* maven  
 \* nexus

Les slides ont été produits avec
[slideshow](http://slideshow.rubyforge.org/) (encore du ruby dedans !).

On peut changer de diapositive avec les flèches, la barre espace, ou le
menu déroulant qui apparaît sous la souris en bas à droite du document.

