Title: Fun with mpd and Nokia N9
date: 2013-04-10
published: true
tags:  N9, nokia, meego, mpd , debian 

One year after buying my nokia N9, I'm still delighted with this phone.

I experimented today a little fun with a distant mpd instance running at home :

[A nice application](http://store.ovi.com/content/205748) allows to acces it remotely from the N9 (on local network, but it worked out nice on 3g data network, with the possibility to start/pause, choose playlist, ...)

Another experiment was to configure the mpd instance to stream out the output on http (see /etc/mpd.conf on debian ). 

Whith the configured port open on the internet, it's possible to receive it on the N9:

With a simple .m3u file that , when doubled tapped from a file browser on the N9, opens the standard music player and plays the music remotely ! (and indicates the track being played).

```bash
#EXTM3U
#EXTINF:dummy
http://your_domain.tld:8000
```

Open source rules !
