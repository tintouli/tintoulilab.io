date: 2010-12-13
tags:  ruby, rails, cantine, lacnr 
title: Rails bar à la cantine


Ce lundi, je suis allé voir une présentation de ruby on rails par
Nicolas Ledez à la [Cantine](http://www.lacantine-rennes.net/).

Un premier contact avec la communauté Ruby rennaise, et l’occasion de
vérifier deux trois trucs :

* est-ce vrai que les programmeurs ruby écrivent toujours leurs tests
avant de coder ? (pas tout à fait)
* est-ce vrai que les programmeurs ruby ont tous des MAC ? (presque)
* est-ce que les programmeurs ruby sont si jeunes qu’ils ne connaissent
pas la
[parenté](http://linuxdevcenter.com/pub/a/linux/2001/11/29/ruby.html) de
leur langage avec [Smalltalk](http://ruby-doc.org/docs/ProgrammingRuby/)
? (Ca c’est pour assurer la ligne éditoriale sur l’ *histoire* des
langages informatiques)

Plus sérieusement, pas mal de monde dans la salle, beaucoup d’étudiants
ou jeunes développeurs, dont certains en ruby/rails, quelques
auto-entrepreneurs, —un miliardaire business angel—, et moi.
Il existe donc [quelques](http://www.dexem.fr/)
[boites](http://www.xilinus.com/) sur Rennes qui font du développement
ruby on rails.

C’était aussi l’occasion, après avoir joué un peu de mon côté avec ruby,
de voir les outils, méthodes, une démo live, et quelques astuces ou
pointeurs intéressants.

Nicolas, qui ne se présente pas comme un développeur, mais comme une
personne qui utilise ruby on rails depuis 6 mois, a fait une
présentation et un tour d’horizon des possibilités du framework, ainsi
que :

* la gestion du code source avec git en local + à distance (repository
privé ou public sous github)
* la gestion des dépendances avec bundler (à ne pas confondre avec
buildr !)
* les tests unitaires / fonctionnels avec junit/rspec/cucumber
* le déploiement (hébergement heroku, ou à la main avec apache +
passenger et capistrano)

un cycle de développement complet, avec la distinction claire entre
environnements de dev/test/production par des fichiers de configuration
générés par le framework rails.

Pour faire très rapide, un environnement de développement ruby on rails,
une fois les outils installés (ruby + gem rails et dépendances) se fait
en quelques lignes :

```bash
rails new TaskManager
cd TaskManager
bundle install
rails server
```
**Et voilà**, un serveur web local intégré permet déjà d’accéder à son
application sur le port 3000 …

Puis on peut raffiner en créant des modèles, controlleurs et leurs vues
associées par quelques commandes supplémentaires :
```bash
rails generate scaffold Reunions title:string description:text
rails generate controller … … …
rails generate view … … …
```
( à compléter)

L’ajout de quelques gems (= librairies) comme `will_paginate` ou
`devise` permettent respectivement d’ajouter simplement de la pagination
ou la gestion d’authentification à l’application.

En fin de soirée, quelques discussions sur les tests ( creuser la
différence entre rspec et cucumber, qui semblent indifféremment utilisés
pour des TUs ou des tests d’intégration).

Nicolas a [mis en ligne](http://www.rennesonrails.com/?p=288) le code de
sa démo, ainsi que quelques pointeurs.

[Les slides](http://www.scribd.com/doc/45336428/Rails-bar-12)

En conclusion, un framework qui permet de développer rapidement des
applications, avec des librairies riches et une communauté active.

Framework
[MVC](http://fr.wikipedia.org/wiki/Mod%C3%A8le-Vue-Contr%C3%B4leur) et
[REST](http://fr.wikipedia.org/wiki/REST) de base.

Merci Nicolas !

* * * * *

Note to self : dans la même veine, voir [Spring
ROO](http://www.springsource.org/roo) ,
[Grails](http://www.grails.org/), et
[playframework](http://www.playframework.org/)
