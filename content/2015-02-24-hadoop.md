Title: Notes hadoop
tags: hadoop
Category: bigdata

! Les [slides](/slide/bigdata/2015-02-24-hadoop.html) sont plus complètes  (work in progress)



# Hadoop : bases

Une explication par le [projet Apache](http://hadoop.apache.org/#What+Is+Apache+Hadoop%3F) lui même :



Je retiens de diverses lectures 2 ( ou 3) grands volets :

* __Le stockage des données, distribué, scalable__. 

Hadoop et ses diverses distributions Cloudera, Hortonworks, Mapr en restent la principale brique, qui semble un socle commun à plein de solutions

Note : dans certains cas de traitements temps réel sans besoin de stockage, on peut s'affranchir d'hadoop

* __Le traitement des données, parallélisable__

Principalement basés sur un stockage des données hadoop, des tas de librairies (Mahout), algorithmes(MapReduce, Spark, ) , gestionnaires (Ambari, Zookeeper) , infrastructures et langages (Hive, Pig, Tez, HBase, Cassandra, Storm).... permettent de paralléliser des traitements de données.

C'est là qu'il y a une grosse ébullition, beaucoup de technlogies proposées, et qu'il y a besoin d'y voir plus clair (Quelle technologie utiliser pour quelle problématique ?).

La maitrise de toutes ces technologies pose question aussi, et je vois un outil comme Talend comme une manière de pouvoir adresser ces technologies de manière homogène, au moins dans les grosses mailles (dans le détail, il y a peut-être des limitations à utiliser une sur-couche comme Talend)

* __La visualisation et la restitution des données__

C'est le troisième point qui est souvent délaissé, mais qui semble évident pour le client : il veut pouvoir manipuler ses données et les rendre lisibles, si possible de manière interactive et par des non experts, qui ne savent pas forcément au départ ce qu'ils sont venus chercher dans ses méga-données.




# Notes diverses :

[source](http://hugfrance.fr/presentation-de-spark-par-tugdual-sarazin/)

__Spark__ : permet de s'affranchir de hadoop/Yarn, fonctionne directement sur Apache Mesos, EC2, ...

__Shark__ : Hive pour Spark (compatible, plus rapide)

[source](http://mahout.apache.org/)

__Mahout : 25 April 2014 - Goodbye MapReduce__ :

Les algorithmes Mahout en MapReduce seront remplacés progressivement par des algos en Spark (au delà de la v0.9, donc)
