Title: Compilation des liens de la semaine 2018-10-14
Date:  2018-10-14
Tags:  veille, spark, timeseries, kafka, angular, cloud, terraform
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine



# [Spark](http://stream.logpickr.com/profile/daniel?_t=rss/tag/spark)
Introducing Flint: A time-series library for Apache Spark - The Databricks Blog
[https://databricks.com/blog/2018/09/11/introducing-flint-a-time-series-library-for-apache-spark.html](https://databricks.com/blog/2018/09/11/introducing-flint-a-time-series-library-for-apache-spark.html)  _[#timeseries](http://stream.logpickr.com/profile/daniel?_t=rss/tag/timeseries)_  









# Autres
Should You Put Several Event Types in the Same Kafka Topic? | Confluent
[https://www.confluent.io/blog/put-several-event-types-kafka-topic](https://www.confluent.io/blog/put-several-event-types-kafka-topic)  _[#kafka](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kafka)_  

Refactoring Angular Apps – How To Keep Angular Apps Clean – Christian Lüdemann IT
[https://christianlydemann.com/refactoring-angular-apps-how-to-keep-angular-apps-clean](https://christianlydemann.com/refactoring-angular-apps-how-to-keep-angular-apps-clean)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  

Découvrir les Cloud Native Languages avec Pulumi | OCTO Talks !
[https://blog.octo.com/decouvrir-les-cloud-native-languages-avec-pulumi](https://blog.octo.com/decouvrir-les-cloud-native-languages-avec-pulumi)  _[#cloud](http://stream.logpickr.com/profile/daniel?_t=rss/tag/cloud)_    _[#terraform](http://stream.logpickr.com/profile/daniel?_t=rss/tag/terraform)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_