Title: Compilation des liens de la semaine 2016-11-20
Date:  2016-11-20
Tags:  veille, bigdata, machinelearning, nosql, graph, cassandra, stream, soa, architecture, spark
Category:  bigdata
Compilation des [liens](https://tintouli.withknown.com/tag/bigdata) de la semaine

# Bases de données Graphe
[3 Advantages of Using Neo4j Alongside Oracle RDBMS](https://neo4j.com/blog/3-advantages-neo4j-alongside-oracle-rdbms)

[Les créateurs de la base graphe Neo4j lèvent 36 M$ - Le Monde Informatique](http://www.lemondeinformatique.fr/actualites/lire-les-createurs-de-la-base-graphe-neo4j-levent-36-m$-66484.html)

[S2Graph](http://s2graph.incubator.apache.org)


# Spark
[$1.44 per terabyte: setting a new world record with Apache Spark - The Databricks Blog](https://databricks.com/blog/2016/11/14/setting-new-world-record-apache-spark.html)

[Music Recommendation service with the Spotify API, Spark MLlib and Databricks – Medium](https://medium.com/@polomarcus/music-recommendation-service-with-the-spotify-api-spark-mllib-and-databricks-7cde9b16d35d#.l6r27u7yx)  _[#machinelearning](https://tintouli.withknown.com/tag/machinelearning)_  

[Spark Summit EU Highlights: TensorFlow, Structured Streaming and GPU Hardware Acceleration](https://www.infoq.com/news/2016/11/spark-summit-eu-2016-recap)


# Bases NoSQL
[A Peek Inside DataStax's New Cloud Strategy](https://www.datanami.com/2016/11/18/peak-inside-datastax-cloud-strategy)  _[#graph](https://tintouli.withknown.com/tag/graph)_    _[#cassandra](https://tintouli.withknown.com/tag/cassandra)_  


# Streaming et Fast Data
[Yelp Open Sources Data Pipeline That Saved It $10M](https://www.datanami.com/2016/11/17/yelp-open-sources-data-pipeline-saved-10m/)  _[#soa](https://tintouli.withknown.com/tag/soa)_    _[#architecture](https://tintouli.withknown.com/tag/architecture)_  

[Intégration de données : Talend entend simplifier le temps réel](http://www.lemagit.fr/actualites/450403159/Integration-de-donnees-Talend-entend-simplifier-le-temps-reel)


# Machine Learning
[Getting started with Machine Learning](https://www.infoq.com/articles/getting-started-ml)








----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_