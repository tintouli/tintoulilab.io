Title: Happy New Year !
Date:  2017-01-01
Tags:  veille, logpickr
Category:  veille

Happy new year for all my dear followers !

Excellente année à tous les lecteurs !

Quelques changements en cette nouvelle année, avec entre autre, quelques modifications sur la

Compilation des liens de la semaine :

* La compilation sera toujours publiée ici
* les liens seront toujours relayés au fur et à mesure sur twitter @tintouli (grâce à l'excellent [feed2tweet]() merci @carlchenet !)
* Ils seront par contre maintenus [ici](https://stream.logpickr.com) au lieu de  [là](https://tintouli.withknown.com), mais toujours avec le logiciel Known qui me sert de moyen de publication/stockage des liens



