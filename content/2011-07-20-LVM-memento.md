title: LVM memento
tags:  RHEL, linux, lvm


lien: <http://doc.ubuntu-fr.org/lvm>

Un petit mémento des principales commandes LVM :  

* liste des commandes relatives aux volumes physiques :  
	
	 man -k pv  
	 pvdisplay
  
* liste des commandes relatives aux groupes de volumes :  
	
	 man -k vg  
	 vgdisplay  

* liste des commandes relatives aux volumes logiques :  
	 man -k lv  
	 lvdisplay

