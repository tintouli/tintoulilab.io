Title: Compilation des liens de la semaine 2016-11-27
Date:  2016-11-27
Tags:  veille, graph, spark, machinelearning, nosql, datascience, virtualisation, conteneur, docker, cloud, stream, flink, container, mesos, couchbase, marathon, angular, reactjs, fonctionnel, tinkerpop, gremlin, orientdb, bigdata, neo4j, nodejs
Category:  bigdata
Compilation des [liens](https://tintouli.withknown.com) de la semaine

# [Bases de données Graphe](https://tintouli.withknown.com/tag/graph)
[Slideshare: How can I use Graphs to better interpet data? | Manning](http://freecontent.manning.com/slideshare-how-can-i-use-graphs-to-better-interpet-data)  _[#spark](https://tintouli.withknown.com/tag/spark)_  

[The 5-Minute Interview: Christophe Willemsen, Senior Consultant at GraphAware](https://neo4j.com/blog/christophe-willemsen-senior-consultant-graphaware)

[OrientDB officially supports TinkerPop 3 - OrientDB Distributed Multi-Model Graph/Document Database](http://orientdb.com/orientdb-officially-supports-tinkerpop-3)  _[#tinkerpop](https://tintouli.withknown.com/tag/tinkerpop)_    _[#gremlin](https://tintouli.withknown.com/tag/gremlin)_    _[#orientdb](https://tintouli.withknown.com/tag/orientdb)_  

[[DevFest Nantes 2016] Découvrir les bases NoSQL orientées graphes avec Neo4j - YouTube](https://www.youtube.com/watch?v=CJ7SA7B6Y3g)  _[#neo4j](https://tintouli.withknown.com/tag/neo4j)_  


# [Spark](https://tintouli.withknown.com/tag/spark)
[Le Blog d'Ippon Technologies - TamTam – SparkZ via @journalduhacker](http://blog.ippon.fr/2016/11/24/tamtam-sparkz)  _[#fonctionnel](https://tintouli.withknown.com/tag/fonctionnel)_  

[Compte rendu du Spark Summit 2016 | OCTO talks !](http://blog.octo.com/compte-rendu-du-spark-summit-2016)

[[DevFest Nantes 2016] Apache Spark avec NodeJS ? Oui, c'est possible avec EclairJS ! - YouTube](https://www.youtube.com/watch?v=qUqmSQ__2QE)  _[#nodejs](https://tintouli.withknown.com/tag/nodejs)_  


# [Bases NoSQL](https://tintouli.withknown.com/tag/nosql)
[From Raw Data to Data Science: Adding Structure to Unstructured Data to Support Product Development](https://www.infoq.com/articles/raw-data-to-data-science)  _[#datascience](https://tintouli.withknown.com/tag/datascience)_  


# [Streaming et Fast Data](https://tintouli.withknown.com/tag/stream)
[Stream Processing Myths Debunked – data Artisans](http://data-artisans.com/stream-processing-myths-debunked)  _[#flink](https://tintouli.withknown.com/tag/flink)_  


# [Machine Learning](https://tintouli.withknown.com/tag/machinelearning)
[Python and R have developed robust ecosystems for data scientists | Opensource.com via @chaica](https://opensource.com/article/16/11/python-vs-r-machine-learning-data-analysis)

[The 10 Best AI, Data Science and Machine Learning Podcasts – Startup Grind – Medium via @manekinekko](https://medium.com/@mattfogel/the-10-best-ai-data-science-and-machine-learning-podcasts-d7495cfb127c#.an5n6xvus)




# Autres
[When choose {React or Angular 2} – Medium via @manekinekko](https://medium.com/@nmamikonyan/when-choose-react-or-angular-2-d4006cd6617c)  _[#angular](https://tintouli.withknown.com/tag/angular)_    _[#reactjs](https://tintouli.withknown.com/tag/reactjs)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_
