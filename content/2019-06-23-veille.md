Title: Compilation des liens de la semaine 2019-06-23
Date:  2019-06-23
Tags:  veille, securite, meetup, scala, deeplearning, AI, angular, test
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine











# Autres
Sécurisez vos applications avec Keycloak | Meetup
[https://www.meetup.com/fr-FR/BreizhJUG/events/262219791](https://www.meetup.com/fr-FR/BreizhJUG/events/262219791)  _[#securite](http://stream.logpickr.com/profile/daniel?_t=rss/tag/securite)_    _[#meetup](http://stream.logpickr.com/profile/daniel?_t=rss/tag/meetup)_  

Scala 2.13.0 · Univalence blog
[https://blog.univalence.io/scala-2-13-0](https://blog.univalence.io/scala-2-13-0)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  

How to work with Subprocesses in Scala
[http://www.lihaoyi.com/post/HowtoworkwithSubprocessesinScala.html](http://www.lihaoyi.com/post/HowtoworkwithSubprocessesinScala.html)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  

models
[https://github.com/rasbt/deeplearning-models](https://github.com/rasbt/deeplearning-models)  _[#deeplearning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/deeplearning)_  

Glossary | Skymind
[https://skymind.ai/wiki/glossary](https://skymind.ai/wiki/glossary)  _[#AI](http://stream.logpickr.com/profile/daniel?_t=rss/tag/AI)_  

Angular: Remplacer Karma/Jasmine par Jest – IneatConseil
[https://blog.ineat-conseil.fr/2019/04/angular-remplacer-karma-jasmine-par-jest](https://blog.ineat-conseil.fr/2019/04/angular-remplacer-karma-jasmine-par-jest)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_    _[#test](http://stream.logpickr.com/profile/daniel?_t=rss/tag/test)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_