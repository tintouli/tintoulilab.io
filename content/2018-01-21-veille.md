Title: Compilation des liens de la semaine 2018-01-21
Date:  2018-01-21
Tags:  veille, angular, scala, proxy, bigdata, kubernetes, docker, machinelearning
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine





# [Machine Learning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/machinelearning)
Cool Breeze of Scala for Easy Computation: Introduction to Breeze Library | Knoldus
[https://blog.knoldus.com/2017/12/27/cool-breeze-of-scala-for-easy-computation-introduction-to-breeze-library](https://blog.knoldus.com/2017/12/27/cool-breeze-of-scala-for-easy-computation-introduction-to-breeze-library)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  





# [Docker](http://stream.logpickr.com/profile/daniel?_t=rss/tag/docker)
Developing Angular applications using Docker – CloudBoost
[https://blog.cloudboost.io/developing-angular-applications-using-docker-6f4835a75195](https://blog.cloudboost.io/developing-angular-applications-using-docker-6f4835a75195)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  



# Autres
The complete guide to Angular Material Themes – Tomas Trajan – Medium
[https://medium.com/@tomastrajan/the-complete-guide-to-angular-material-themes-4d165a9d24d1](https://medium.com/@tomastrajan/the-complete-guide-to-angular-material-themes-4d165a9d24d1)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  

Awesome SBT plugins for everyone
[http://tech.ovoenergy.com/awesome-sbt-plugins-for-everyone](http://tech.ovoenergy.com/awesome-sbt-plugins-for-everyone)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  

Otoroshi
[https://maif.github.io/otoroshi/manual](https://maif.github.io/otoroshi/manual)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_    _[#proxy](http://stream.logpickr.com/profile/daniel?_t=rss/tag/proxy)_  

Les pipelines de big data modernes avec Kubernetes
[https://www.infoq.com/fr/news/2018/01/big-data-pipelines-kubernetes](https://www.infoq.com/fr/news/2018/01/big-data-pipelines-kubernetes)  _[#kubernetes](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kubernetes)_  

Compodoc - The missing documentation tool for your Angular application
[https://compodoc.github.io/website/guides/tutorial.html](https://compodoc.github.io/website/guides/tutorial.html)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  

Scaladex
[https://index.scala-lang.org](https://index.scala-lang.org)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  

Disable your non FP code by Scalafix (installation guide)
[https://vovapolu.github.io/scalafix/2017/12/14/scalafix-fp-alpha.html](https://vovapolu.github.io/scalafix/2017/12/14/scalafix-fp-alpha.html)  _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  

Demystifying dynamic Forms in Angular
[https://juristr.com/blog/2017/10/demystify-dynamic-angular-forms](https://juristr.com/blog/2017/10/demystify-dynamic-angular-forms)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_