#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Tintouli'
SITENAME = u'Don\'t Panic !'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

THEME = 'extra/pelican-twitchy'
SITESUBTITLE = 'Coding or not coding ?'
#OPEN_GRAPH = True
BOOTSTRAP_THEME = "superhero"
PYGMENTS_STYLE = "manni"
#SITELOGO = 'extra/chouette.png'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Veille Big Data', 'https://tintouli.withknown.com/tag/bigdata'),
	  ('Journal du hacker','https://www.journalduhacker.net/about'),
	  ('Mégadonnées sur le Jdh','https://www.journalduhacker.net/t/m%C3%A9gadonn%C3%A9es'),)

# Social widget
SOCIAL = (
          ('Archives', '/archives.html'),
	  ('Tags', '/tags.html'),
	  ('Veille Big Data', 'https://tintouli.withknown.com/tag/bigdata'),
	  ('Journal du hacker','https://www.journalduhacker.net/about'),
	  ('Twitter', 'https://twitter.com/tintouli'),
	  ('Diaspora', 'https://framasphere.org/people/93f118a0d88401312c3f2a0000053625'),
          )

DEFAULT_PAGINATION = 15

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
